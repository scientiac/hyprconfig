-- Pull in the wezterm API
local wezterm = require("wezterm")
local mux = wezterm.mux

-- This table will hold the configuration.
local config = {}

-- wezterm.on("gui-startup", function(cmd)
-- 	local tab, pane, window = mux.spawn_window(cmd or {})
-- 	window:gui_window():maximize()
-- end)
--
-- wezterm.on("gui-attached", function(domain)
-- 	-- maximize all displayed windows on startup
-- 	local workspace = mux.get_active_workspace()
-- 	for _, window in ipairs(mux.all_windows()) do
-- 		if window:get_workspace() == workspace then
-- 			window:gui_window():maximize()
-- 		end
-- 	end
-- end)

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
	config = wezterm.config_builder()
end

-- This is where you actually apply your config choices

-- For example, changing the color scheme:
config.color_scheme = "Catppuccin Mocha"

config.window_decorations = "NONE"

-- Font style
config.font = wezterm.font("FantasqueSansMono Nerd Font Mono")

-- Hides the tab bar when there is only one tab
config.hide_tab_bar_if_only_one_tab = true

-- Terminal Opacity
config.window_background_opacity = 0.8

-- and finally, return the configuration to wezterm
return config
