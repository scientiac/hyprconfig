#! /usr/bin/bash

# Assign variables to file paths saperated by space.
paths="/home/scientiac/.config/alacritty/ /home/scientiac/.config/eww/ /home/scientiac/.config/dunst/ /home/scientiac/.config/hypr/ /home/scientiac/.config/rofi/ /home/scientiac/.config/waybar/ /home/scientiac/.config/cava/ /home/scientiac/.config/wezterm/"

# Get filenames from the added paths
files=$(basename -a -z $paths | tr '\0' ' ')

# Bring everything to the dotfiles directory on gitlab.
cp -r $paths /home/scientiac/Experiments/gitsync/hyprconfig/

echo "Copied [ "$files"] to /home/scientiac/Experiments/gitsync/dotfiles/"

sleep 1

# Git add all the files that were borught to the dotfiles directory.
git add .
echo "Executed 'git add' on [ "$files"]!"

sleep 1

# Git commit with a message.
git commit -m "Hypr Backup: Sync"
echo "Executed 'git commit' with message [Regular Backup: Sync]!"

sleep 1

# Git Push
git push git@gitlab.com:scientiac/hyprconfig.git
echo "Executed 'git push'!"

sleep 1
echo "done!"
