# hyprconfig
## My Hyperland configurations.

### Dependencies 

- Distro: `fedora`
- Terminal: `alacritty`
- Bar: `waybar`
- Notification Daemon: `dunst`
- Lock: `swaylock-effects`
- Idle: `swayidle`
- Night Mode: `wlsunset`
- Wallpaper: `swaybg`
- Launcher: `rofi-wayland`
- Emojis: `bemoji`
- Screenshot Tool: `grim`, `slurp`
- Workspace Switching: `socat`
- Clipboard: `wl-clipboard`

### Screenshots

![1](_res/screenshot.png)
![2](_res/screenshot_2.png)

### Build Dependencies
> for Hyperland and Waybar on fedora

```

ninja-build
cmake
meson
gcc-c++
libxcb-devel
libX11-devel
pixman-devel
wayland-protocols-devel
wayland-devel
libwayland-server
cairo-devel
pixman-devel
egl-wayland-devel
egl-wayland
libwayland-egl
mesa-libEGL-devel
libdisplay-info
libdisplay-info-devel
rust-libudev-devel
wlroots-devel
xdg-desktop-portal-wlr
libdrm-devel
xorg-x11-server-Xwayland
xorg-x11-server-Xwayland-devel
hwdata-devel
libliftoff
libliftoff-devel
rust-xcb-devel
xcb-util
xcb-util-devel
uuid-devel
libuuid-devel
qt6-qtwayland-devel
fmt-devel
spdlog-devel
gtkmm3.0-devel
libdbusmenu-gtk3-devel
jsoncpp-devel
libnl3-devel
upower-devel
playerctl-devel
rust-libpulse-sys-devel
libevdev-devel
libmpdclient-devel
wireplumber-devel
pipewire-jack-audio-connection-kit-devel
gtk-layer-shell-devel
scdoc
date-devel

```
