#!/bin/env bash

# Options for powermenu
lock=""
logout=""
shutdown=""
reboot=""
sleep=""

# Get answer from user via rofi
selected_option=$(echo "$lock
$logout
$sleep
$reboot
$shutdown" | rofi -dmenu\
                  -i\
                  -p "Power"\
		  -theme "~/.config/rofi/applets/powermenu/powermenu.rasi")
# Do something based on selected option
if [ "$selected_option" == "$lock" ]
then
    ~/.config/hypr/scripts/lock.sh
elif [ "$selected_option" == "$logout" ]
then
    loginctl terminate-user `whoami`
elif [ "$selected_option" == "$shutdown" ]
then
    poweroff
elif [ "$selected_option" == "$reboot" ]
then
    reboot
elif [ "$selected_option" == "$sleep" ]
then
    wlsunset
else
    echo "No match"
fi
