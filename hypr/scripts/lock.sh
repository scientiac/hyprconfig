swaylock \
	-i ~/Pictures/wallpapers/everforest_logo.png \
	--font FantasqueSansMono Nerd Font Mono --timestr %H:%M --datestr %A --indicator \
	--inside-color 00000000 --separator-color 00000000 --inside-ver-color 00000000 --inside-clear-color 00000000 --inside-wrong-color 00000000 --line-color 00000000 --line-ver-color 00000000 --line-wrong-color 00000000 --line-clear-color 00000000 --ring-ver-color 000000 --ring-wrong-color FF0000 --ring-color 000000 --text-color 00000000 --text-ver-color 00000000 --text-wrong-color 00000000 --text-clear-color 00000000 --indicator-radius 120 \
	--indicator-thickness 15 \
	--fade-in 0.2
